# -*- coding: utf-8 -*-
"""
Created on Fri May 13 15:33:48 2016

@author: janp
"""
import sys
import os
import argparse

parser=argparse.ArgumentParser(description=
'''
This program creates plots from '.mpf' files containing data from the vivana model 
or its verification experiments. These plots are then inserted into a report.
The plot module takes in an unlimited number of .mpf files. Matrices from different
 ".mpf" files which have the same matrix name are plotted together.'''
)
parser.add_argument('-i',"--in_path",help=
'''----Path of input ".mpf" files---- If this optional argument is
 not used the program looks for ".mpf" files in the "Input" folder'''
 )
parser.add_argument('-o',"--out_path",help=
'''----Path to save plots---- If this optional argument is not used
 the program saves the plots in the "Output" folder'''
 )
parser.add_argument('-c','--client',help='Client name (optional)')
parser.add_argument('-l','--mpf_list',nargs='+' ,help=
'''----Name(s) of input ".mpf" files in input directory---- Use space to seperate
 file names. In case this optional argument is not used all ".mpf" files in the
 directory are used as input files. The order of the list is expected to be:
 [ Experiment mpf , latest Vivana mpf , older Vivana mpf]. The number of entries
 is not fixed.'''
 )
parser.set_defaults(vivana_sequence=False)
args=parser.parse_args()

wdir=os.getcwd()
sys.path.insert(0, wdir+'\\Modules\\')

import plot_program_flex
import render_latex

#plot_program_flex.plot_mpf('C:\\Users\\janp\\Documents\\datasets\\exp_viv_nan\\plot_unlike',"C:\\Users\\janp\\Documents\\test_python")
if args.in_path==None:
    in_path=wdir+'\\Input'
else:
    in_path=args.in_path
    
if args.out_path==None:
    out_path=wdir+'\\Output'
else:
    out_path=args.out_path

    
work_path= wdir+'\\Working_folder'

tex_name='rapport_auto.tex'
tex_path=wdir+'\\Latex'


plot_program_flex.plot_mpf(in_path,work_path,args.mpf_list,True)



render_latex.renderer(tex_path,tex_name,work_path,out_path,args.client)




