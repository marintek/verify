-----------
###########
Read Me
###########
-----------

###########
Description
###########

This set of programs consists of the executable programs "command_plot" as well as "command_plot_latex"

#######command_plot_latex############

This program creates plots from '.mpf' files containing data from the vivana model 
or its verification experiments. These plots are then inserted into a report.
The plot module takes in an unlimited number of .mpf files. Matrices from different
 ".mpf" files which have the same matrix name are plotted together.


#####command_plot####
This program takes in an unlimited number of .mpf files and visualises them
by creating one plot for each matrix. Matrices from different ".mpf" files
which have the same matrix name are plotted together. When executed an overview
about all mpf files as well as their matrix names is printed.
Additionally it offers the opportunity to create scatterplots of the Vivana data.
These can be very useful for correlation analyses.


#######
Folders
#######

All executable programs are in the main folder and always start with the term "command".
They can be run from the command line window.
When using the command "-h" an overview over all further commands as well as a program description will be printed out.


---------
Subfolders
---------

--Input--
When not changing the input directory by using the command "-i" this is where the program looks
for the input ".mpf" files. All of them are chosen when not selecting specific files using the command "-l"

--Output--
This is where in case of "command_plot_latex" the report is saved as a pdf or in case of "command_plot" 
the plots are saved.

--Modules--
All self written python models used by the executable programs are saved here.
Additionaly there is a test program, which creates an mpf file to test the scatter function.


--Working_Folder--
In this folder all automatically created files are located which means that "command_plot_latex"
 saves all the the data there which is required in order to create the report.

--Latex--
In this folder the latex document is saved from which the report is created. For further information 
open the "readme" in this folder



################
Installation
################


#######Anaconda#########
Anaconda with Python version 3.5 :
https://www.continuum.io/downloads

Anaconda is a python distribution including the most commonly used python modules


############miopyc###############
https://bitbucket.org/marintek/miopyc

miopyc is a python module, which reads the sintef intern matrix plot file format '.mpf'
Make  sure, that all 'Not a numer' values are called 'nan'


########Miktex#############
Miktex is a texdistribution 
http://miktex.org/howto/install-miktex
(The installation might be complicated for mac os users)

In case you would like to change the Latex- document read the "Readme" file in the 'Latex' folger

Out of the box Miktex uses Pdftex in order to compile the Pdf document.
The python programs however uses Xelatex which is also a part of the MikTex distribution.


#######Sintef Latex Templates#######
Paste the Sintex folder into the following path:

MiKTeX 2.9\tex\latex







