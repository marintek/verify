# -*- coding: utf-8 -*-
"""
Created on Fri May 13 15:33:48 2016

@author: janp
"""
import sys
import os

file_dir = os.path.realpath(__file__)
sys.path.append(os.path.join(file_dir,'Modules'))


import argparse
import os

parser=argparse.ArgumentParser(description=
'''This program takes in an unlimited number of .mpf files and visualises them
 by creating one plot for each matrix. Matrices from different ".mpf" files
 which have the same matrix name are plotted together. When executed an overview
 about all mpf files as well as their matrix names is printed.
 Additionally it offers the opportunity to create scatterplots'''
 )
parser.add_argument('-i',"--in_path",help=
'''----Path of input ".mpf" files---- Use either Slash "/" or double backlash 
"\\\\". No blanc spaces are allowed in the path. If this optional argument is
 not used the program looks for ".mpf" files in the "Input" folder'''
 )
parser.add_argument('-o',"--out_path",help=
'''----Path to save plots---- Use either Slash "/" or double backlash "\\\\".
 No blanc spaces are allowed in the path. If this optional argument is not used 
 the program saves the plots in the "Output" folder'''
 )
parser.add_argument('-l','--mpf_list',nargs='+' ,help=
'''----Names of input ".mpf" files in chosen directory---- Use space to seperate
 file names. In case this optional argument is not used all ".mpf" files in the
 directory are used as input files.''' )
parser.add_argument('-v','--vivana_sequence', dest='vivana_sequence', action='store_true', help=
'''---This argument can be activated when plotting vivana related ".mpf" files 
in order to make better looking plots--- For this argument an ordered mpf_list 
is expected as an input when using the command "-l". The order of the list is
 expected to be: [ Experiment mpf , latest Vivana mpf , older Vivana mpf].The
 number of entries is not fixed. This optional boolean argument does not require 
 any further inputs.'''
 )
parser.add_argument('-s','--scatter', dest='scatter', action='store_true',help=
'''---When this boolean argument is activated, the program will create scatter
 plots --- These are mapping the y_values of experimential data onto the vivana 
 data. Make sure to use the term "Experiment" in the legend names of the 
 experimential data. A linear interpolation is applied to the vivana data in order
 to be able to create the plots (further documentation in the "Scatter_functions.py" 
 module )''')

parser.set_defaults(vivana_sequence=False, scatter=False)
args=parser.parse_args()



wdir=os.getcwd()
sys.path.insert(0, wdir+'\\Modules\\')

import plot_program_flex


#plot_program_flex.plot_mpf('C:\\Users\\janp\\Documents\\datasets\\exp_viv_nan\\plot_unlike',"C:\\Users\\janp\\Documents\\test_python")
if args.in_path==None:
    in_path=wdir+'\\Input'
else:
    in_path=args.in_path
    
if args.out_path==None:
    out_path=wdir+'\\Output'
else:
    out_path=args.out_path

    
work_path= wdir+'\\Working_folder'

tex_name='rapport_auto.tex'
tex_path=wdir+'\\Latex'

#plot_program_flex.plot_mpf('C:\\Users\\janp\\Documents\\datasets\\exp_viv_nan\\plot_unlike',"C:\\Users\\janp\\Documents\\test_python")

plot_program_flex.plot_mpf(in_path,out_path,args.mpf_list,args.vivana_sequence,args.scatter)
