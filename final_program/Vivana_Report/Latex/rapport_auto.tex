\documentclass[unrestricted,unrestrictedsummary]{sintefreport}


\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{natbib} 
\usepackage{siunitx}
\usepackage{threeparttable}
\usepackage{booktabs}
\usepackage{titleref}
\usepackage{float}
\usepackage{hyperref}

\graphicspath{{../Working_folder/}}
\graphicspath{{../Latex/}}


\title{The \texttt{sintefreport} class}
\subtitle{A \XeLaTeX\ class}
\version{1.0}
\reportnumber{1337}
\author{Elizabeth Passano, Jie Wu, Carl M. Larsen}
\client{???client}
\project{Proceedings of the 29th International Conference on Ocean, Offshore and Arctic Engineering\\
	OMAE 2010\\
	June 6-11, 2010, Shanghai, China
	}
\attachmentpages{}

\abstract{
abstract
	
	
	}
\prepared{Federico Zenith}
\titlefigure{
  \begin{center}
  
    \Huge VIV OF FREE SPANNING PIPELINES: COMPARISON OF RESPONSE FROM     SEMI-EMPIRICAL CODE TO MODEL TESTS

  \end{center}
}
\keywords{Vivana\\Test\\Experiment} 


\bibliographystyle{myabbrvnat}
\begin{document}

\frontmatter	


\section*{Summary} \addcontentsline{toc}{section}{Summary}
	The purpose of this paper is to compare predictions of vortex-induced vibrations (VIV) from a semi-empirical program to experimental data. The data is taken from a VIV model test program of a free span pipeline using a long elastic pipe model. Both in-line (IL) and cross-flow (CF) vibrations are compared.
	The Norwegian Ormen Lange field development included pipelines laid on very uneven seafloors, resulting in many free spans. As part of the preparations for this field development, VIV model tests of single- and multi-span pipelines were carried out at MARINTEK for Norsk  Hydro, which later became a part of Statoil.
	The VIVANA program is a semi-empirical frequency domain program based on the finite element method.  The program was originally developed by MARINTEK and the Norwegian University of Science and Technology (NTNU) to predict cross-flow response due to VIV. The fluid-structure interaction in VIVANA is described using added mass, excitation and damping coefficients. Default curves are available or the user may input other data.
	VIVANA originally included only cross-flow excitation but pure in-line excitation was later added. Recently, simultaneous cross-flow and in-line excitation has also been included. At present, the excitation in the cross-flow and in-line directions is not coupled. Coefficients for simultaneous cross-flow and in-line excitation have been proposed and are available in VIVANA.
	In this paper, response predicted by VIVANA has been compared to the Ormen Lange model tests for selected test series.  The analyses with pure IL loading gave good estimates of IL response up to and beyond the start of CF response.
	The analyses with combined CF and IL loading gave good response estimates for the test series with a long span. The experiments with short spans tended to give CF and IL mode 1 response while the present version of the program gave IL response at higher modes. 
	The present coefficient based approach is, however, promising. Further work should aim at establishing better coefficients and to understanding the interaction between CF and IL response.
	
\newpage



\section{Experiment Series 1}
\subsection{Introduction}
\subsubsection{Test Arrangement}
The test arrangement is described by \citep{huse}. Figure 1 shows the test set-up. A 12.0 m truss girder serves as the support structure for the pipe. At one end there is the mechanism for pretension and variation of the horizontal stiffness at the support. The other end is instrumented for axial force measurement. A special arrangement of movable supports was made for adjusting the active free span length of the model. Both single span and multi span pipelines could be tested by adjusting the positions of these supports.
The pipeline model was instrumented with ten sets of strain gauges that made it possible to measure bending strains for IL and CF displacements. Displacements along the pipe were found by conventional modal analysis. Hence, snapshots of dynamic displacements at given points of time can be presented and also time series of CF and IL response and modal participation factors for both directions. The procedure is described by Moe and Solaas (2002).
In total three phases of tests were carried out. The same pipe model was used in the first two phases. It was modified for the third phase according to change in the specifications of the real Ormen Lange pipeline. Key data for the two models are found in Table \ref{key data}.

\begin{figure}[H]
	\centering
	\includegraphics[width=8.5cm]{line_graph.png}
	\caption{Experimential set-up}
	\label{exp set}
\end{figure}

\begin{table}[H]
	
	\centering
	\caption{Key data for pipe models}
	\label{key data}

	\begin{tabular}{l|c c c}
		
		\toprule
		
		
		Parameter &	Model  case 10 and 42 & Model case 75 &	Units \\
		
		\hline 
		
		Length $L$ &	$11.413$ &	$11.413$ &	\si{\meter}\\
		Diameter $D$ &	$0.0326$ &	$0.03504$ &	\si{\meter} \\
		Bend. Stiffness $EI$ &$	0.203$ &	$0.203$ & \si{\kilo \newton \metre \squared}\\
		Axial stiffness $EA$	&$5000$ &	$5000$ &	\si{\kilo \newton}\\
		Tension $NEFF$ &	$0.05 - 0,07$ &	$0.05$ &	\si{\kilo \newton}\\
		Submerged weight $w$ &	$3.25$ &	$3.61$ & \si{\newton \per \metre}\\
		Mass $m$ &	$1.147$ &	$1.307$ &	\si{\kilogram \per \metre}\\
		End spring stiff $k$ & $34.4$ &	$9.75$ &	\si{\kilo \newton \metre}\\
		
		\bottomrule
	\end{tabular}

\end{table}



Three series of experiments were selected for the case study, series 10, 42 and 75. The two first series were with the Phase I/II model, while the last was with the Phase III model.
The series 10 experiments were carried out without clamps, giving a free span length of $11.413 \si{metre}$. A static tension of $67.8 \si{\newton}$ was applied.
In series 42, six sets of clamps were applied leaving a free span of $4.729 \si{\metre}$. A static tension of $51.5 \si{\newton}$ was applied.
In series 75 the model was clamped at app. $0.5 \si{\metre}$ intervals on either side of the free span. The model had a static tension of $50.0 \si{\newton}$.

\subsubsection{Case Study}
The finite element model of the pipeline model used in the experiments consists of 116 beam elements with lengths of $7 – 11 \si{\centi \metre}$. A horizontal spring is applied at one end and a static tension applied. The clamps are modeled by pairs of translational springs perpendicular to the line.
The flow velocity in the analyses was varied at intervals of $0.01 \si{\metre \per \second}$ through the range of towing speeds for each series.
Analyses were performed with two different options for the VIV loading; with only IL VIV loads and with the new option for combined CF and IL VIV loads. Note that in addition to determining the VIV load coefficients, this option also determines how the response frequencies are found, see above. The option for pure IL VIV loading is intended for the pure IL response seen at low current levels. 
The CF and IL response frequencies, the mode numbers of the eigenfrequencies selected as response frequencies, the maximum CF and IL response amplitudes and the CF and IL added mass are presented for each test series. Snapshots are also presented for a few selected cases. 



\subsection{Comparison to experimental data}
Key results from the analyses of series 10 are compared to results from the model tests in Figure \ref{freq ser10}, Figure \ref{mode ser10} and Figure \ref{amp ser10}. The analysis results are presented with lines and small symbols while the test results are marked with larger triangles. CF results are red and IL results are blue and green.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{Figure1.png}
	\caption{Response frequencies from the model tests and analyses of test series 10}
	\label{freq ser10}
\end{figure}

The CF response frequencies from the analyses (red line) are in very good agreement with the test results (red triangles). The frequencies from the analyses with pure IL loading (green line) are in good agreement with the two tests with only IL response (blue triangles). Above $0.06 \si{\metre \per \second}$ the IL response frequency from the analyses with CF and IL loading (blue line) are in good agreement for the five tests where the IL response frequency is twice the CF frequency. The model tests at $0.076 \si{\metre \per \second}$ and $0.104 \si{\metre \per \second}$ gave IL frequencies of three times the CF frequencies. The analyses are based on the assumption that the IL frequency is twice the CF frequency and can therefore not reproduce this combination of frequencies.

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{Figure2.png}
	\caption{Response modes from the model tests and analyses of test series 10}
	\label{mode ser10}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=10cm]{Figure3.png}
	\caption{Maximum response amplitudes from the model tests and analyses of test series 10. Normalized by pipe diameter}
	\label{amp ser10}
\end{figure}

The CF modes from the analyses correspond quite well to the modes from the experiments; both go through mode 1, 2 and 3. In-line, both analyses and model tests go through modes 1, 2, 3 and 4 for the current range of the experiments. The IL mode number from the analyses increases in steps with increasing current, while the IL mode number found from the experiments has a less clear trend.
The maximum IL response from the analyses is in reasonably good agreement with the experiments, but tends to be somewhat lower. 
The analyses give somewhat higher CF response than the experiments. The differences are largest for the test at $0.076 \si{\metre \per \second}$. This test has less CF response than IL response and the IL frequency was three times the CF frequency. The analyses had CF mode 1 and IL mode 2 in this region $(0.06 – 0.09 \si{\metre \per \second})$ and got CF amplitudes up to $1.03 D$. 
In the present version of VIVANA, the response is calculated independently at the CF and IL frequencies. In the experiments, there will be interaction between the loading and response at different frequencies. CF mode 1 and IL mode 2 may not be a combination that interacts well?



\bibliography{Vivana_bib}
\backmatter



\end{document}
