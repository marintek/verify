------------
############
LaTex Readme
############
------------



This folder contains all static files which are necessary in order for "command_plot_latex.py" 
to create the report.
This includes the reference library "Vivana_bib.bib", the library type "myabbrvnat.bst" as well
as all static pictures embedded in the tex document.

###########
Program Execution
###########


The program reads "rapport_auto.tex", changes it and then saves it in the "Working_Folder".
"myabbrvnat.bst" as well as "Vivana_bib.bib" are copied into "Working_Folder" as well where 
the newly created "rapport_auto_rendered.tex" is executed. The resulting ".pdf" file is then
copied into the output folder.


###################
Changing the Report
###################

In case you want to change the report it is recommeded to install a Tex writing environment like TexStudio
---------------------------------
-----Install TeXStudio-----------
---------------------------------
http://www.texstudio.org/

When executing the LaTex script texstudio, it will compile the script using PdfLatex.
"command_plot_latex.py" however uses xelatex. Therefore it is recommended to make texstudio run 
Xelatex instead.
The easiest way of achieving this is to click on the "Options" button on the upper strp in the 
"TexStudio" window. Then choose "Configure latex". Choose "commands" in the window, which appears then,
and simply change the commando "PdfLatex" to: "xelatex -interaction=nonstopmode %.tex"

------------------
------Latex-------
------------------

"command_plot_latex.py" replaces the term "???client" with the name of the customer in "rapport_auto.tex". 
The name of the customer can be defined using the command "-c" when executing "command_plot_latex".

The references can be defined in the file "Vivana_bib.bib". Every entry is following a strict format which is 
defined by "myabbrvnat.bst".
Examples of these can be found here: https://verbosus.com/bibtex-style-examples.html?lang=de
This webpage gives an overview over the citation commands in the latex script : https://de.sharelatex.com/learn/Natbib_citation_styles







