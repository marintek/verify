# -*- coding: utf-8 -*-
"""
Created on Fri Jun 10 17:53:04 2016

@author: janp
"""
import numpy as np
import matplotlib.pylab as plt
import re
   
'''
---------------------
Functions for scatter plot
----------------------
'''

'''
When making the scatter plots, I faced the problem, that the vivana data has different 
x-values than the experimential data:

Vivana:     #     #     #     #      #       #         #

x-val:      1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18

Experiment:   #            #             #      #    #

Therefore I decided to write these functions, that apply a linear interpolation 
between the vivana values in order to calculate the y values of the vivana data 
from x-values of the experimential data.

'''
   

'''
find_closest_smaller(in_val,arr) finds the value in "arr" which is closest to in_val and smaller than in_val
and returns its position in arr. 
arr is expected to be sorted already beginning with the smallest and ending with the biggest value
'''
def find_closest_smaller(in_val,arr):
    if not (arr==np.sort(arr)).all():
        print('Warning: x_values of the matrix are not sorted')
        
    for i in range(len(arr)):
        if in_val <= arr[i]:
            return (i)
            
'''
solve_equ(x1,x2,y1,y2,x_new) calculates the coefficients of a linear function containing 
the points [x1,y1] and [x1,y2] and returns the y value belonging to x_new according 
to this linear function
'''            
def solve_equ(x1,x2,y1,y2,x_new):
    equ=np.array([[x1,1],[x2,1]])
    res=np.array([y1,y2])
    a,b=np.linalg.solve(equ,res)
    y_new=a*x_new+b
    return y_new
    
'''
linear_interpolation(exp_x_vals,viv_x_vals,viv_y_vals) creates a linear interpolation between 
the datapoints of the vivana test and calculates new y_values of the vivana data for the x_values
that where measured in the experiments. This way it returns an array of Vivana y values which has
the same length as the array containing the experiment y values  

'''    
def linear_interpolation(exp_x_vals,viv_x_vals,viv_y_vals):
    viv_y_new=[]
    
    for val in list(exp_x_vals):
        ind=find_closest_smaller(val,viv_x_vals)
        x1=viv_x_vals[ind]
        y1=viv_y_vals[ind]
        try:
            x2=viv_x_vals[ind+1]
            y2=viv_y_vals[ind+1]
        except IndexError:
            print('''
            There occured an error when interpolating the vivana 
            data for the scatter plots. The program expects more Vivana
            data points than Experiment data points''')
         
        viv_y_new.append(solve_equ(x1,x2,y1,y2,val))
    return viv_y_new
    

            
def test_for_scatter(plot_list):
    error=False
    if len(plot_list)<=2:
        error=True 
    else:
        for i in range (len( plot_list)-1):
            if not len(plot_list[i].yvalues)==(plot_list[i+1].yvalues):
                error=True
    return error

'''
extract_and_interpolate(plot_list)
Expects a plot list containing an mpf object containing experimental data as
well as an mpf object containing vivana data as input. 
It interpolates the vivana data and outputs yvalues of vivana and experimental
data with the same length so it can be used to create a scatter plot.
When i==0 The IL values are extracted, when i==1 the cf values are extracted
'''

def extract_and_interpolate(plot_list,i)  :
    
 
    
    x_axis=[]
    y_axis=[]
    for j in range (2): #skips between EXP and VIV mpf file
        y_ord_dit=plot_list[j].yvalues
        y_vals=list(y_ord_dit.items())[i]
        y_axis.append(y_vals)
        
        
        x_vals=plot_list[j].xvalues
        x_axis.append(x_vals)
    
    exp_int=None
    for k in range(2):   #Figures out wether experimental data is 1st or 2nd place in axis
        if 'xperiment' in y_axis[k][0]:
            exp_int=k
            
    if exp_int==None:
        exp_int=0
        print('Could not figure out which dataset is experimental dataset, please use "Experiment" in legend names')
    if exp_int==0:
        viv_int=1
    
    else:
        viv_int=0
    
    #---y values of MPF ----
    exp_y_ax=y_axis[exp_int]
    viv_y_ax=y_axis[viv_int]
    exp_y_label=exp_y_ax[0]
    viv_y_label=viv_y_ax[0]
    exp_y_vals=exp_y_ax[1]
    viv_y_vals=viv_y_ax[1]
    
    #----x-values of MPF-----
    exp_x_ax,viv_x_ax=x_axis
    exp_x_vals=exp_x_ax
    viv_x_vals=viv_x_ax
    
    viv_y_new=linear_interpolation(exp_x_vals,viv_x_vals,viv_y_vals)            
    
    return  exp_y_vals,viv_y_new, exp_y_label , viv_y_label                            


'''
scatter_plot(plot_list ,exp_y_vals,viv_y_new, exp_y_label , viv_y_label)
creates scatter plots


'''
def scatter_plot(output_path, plot_list ,exp_y_vals,viv_y_new, exp_y_label , viv_y_label):
        if 'il' in viv_y_label.lower():
            direction='IL'
        elif 'cf' in viv_y_label.lower():
            direction='CF'
        else:
            direction=viv_y_label
        
        ax=plt.scatter(exp_y_vals,viv_y_new,s=50)
        
        plt.xlabel(exp_y_label)
        plt.ylabel(viv_y_label)
        if plot_list[0].attrs['TITLE']==plot_list[1].attrs['TITLE']:
            plt.title(plot_list[0].attrs['TITLE'])
        
        ylim=plt.ylim()
        #xlim=ax.get_xlim()
        #ylim=ax.get_ylim()
        step=(ylim[1]-ylim[0])/10
        ax=plt.plot(np.arange(ylim[0],ylim[1],step),np.arange(ylim[0],ylim[1],step),'r',linewidth=1.5)
        
        savetitle=re.sub('[^0-9a-zA-Z]+', '_',plot_list[0].attrs['TITLE'])+' '+direction
            
        plt.savefig(output_path+savetitle+'_Scatter'+'.png',bbox_inches='tight',dpi=400)
        plt.draw()        
        plt.close()