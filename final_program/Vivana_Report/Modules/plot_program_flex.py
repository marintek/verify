# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 15:17:32 2016

@author: janp
"""
import numpy as np
import pandas as pd
import pydoc
import miopyc as mp
import matplotlib.pyplot as plt
import re
import os
from os import listdir
from os.path import isfile, join
import sys

wdir=os.getcwd()
sys.path.insert(0, wdir)

import Scatter_Functions as scat


def plot_mpf(input_path,output_path,mpf_list=None,viv_seq=False,scatter=False):
    
    
    wdir=os.getcwd()
    if output_path==None:
        output_path=wdir
    if input_path==None:
        input_path=wdir
        
    path=input_path+'\\'
    
    output_path+='\\'
    
    
    '''
    ---Function description---
    df=check_keys(Keys_df)
    
    This function returns a dataframe, with the name of every matrix name / key on
    the y axis and the title of every mpf file on the x axis.
    It describes whether the current matrix title is included in the current 
    mpf file by using boolean variables.
    '''
    def check_keys(keys_df):
        keys=np.array(keys_df)
        #plotted=[]
        plot_boo=[]
        plot_key_names=[]
        for i in range(np.shape(keys)[0]):
            for j in range(np.shape(keys)[1]):
                key=keys[i,j]
                if key==None:
                    continue
                
                if key in plot_key_names:
                    #einen weiter in der schleife                
                    continue
                plot_key_names.append(key) 
                
                #loop_range=np.delete(np.arange(np.shape(keys)[0]),i)
                boo=np.empty(np.shape(keys)[0])
                for k in range(np.shape(keys)[0]):
                    boo[k]= key in keys[k]
                    
                plot_boo.append(boo)
        df=pd.DataFrame(plot_boo,index=plot_key_names,columns=keys_df.index.tolist())
        return df
                        
    
                    
                    
                
    '''
    filenames(path,file_ending)
    
    Function returns list of all filenames in the directory given by 'path' 
    with the file ending given by file_ending
    
    '''
    def filenames(path,file_ending):

        onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
            
        onlyfiles_ending=[]
        for name in onlyfiles:
            if file_ending in name:
                onlyfiles_ending.append(name)
            
        return onlyfiles_ending

        
    def test_attrs(exp_sec,viv_sec):
        atts=['TITLE','XLABEL','YLABEL']
        
        for att in atts:
            if not exp_sec.attrs[att]==viv_sec.attrs[att]:
                print('Datasets titled: "'+viv_sec.attrs['TITLE'] +'" have different '
                +att+' names for Vivana and experimential data:'+'\n'+att+
                ' of Vivana data is called "'+viv_sec.attrs[att]+'"\n'+att+
                ' of the experimential data is called "'+exp_sec.attrs[att]+'"\n'+
                att+' is now named "'+viv_sec.attrs[att]+'" in the plot' )
                
        
    
    
           
    '''
    Function description:
    mat_plt_list(plot_list)
    
    This function creates a plot by going through every entry in plot_list, where every entry
    represents a matrix from a different mpf-file. Then again every column of the matrix is plottet. 
    Every call of the function creates one plot.
    '''
    def mat_plt_list(plot_list):
        global plot_num
        i=0
        for sec in plot_list:
                      
            
            style_list=[['<','>','>-'],['+-','+-','^-'],['--','-.',':']]
            color_list=[['b','r','g'],['r','b','g'],['r','b','g']]
                
        
        
            x_vals=sec.xvalues
            y_vals=sec.yvalues
            
            plot_df=pd.DataFrame(index=x_vals, data=y_vals)
            global lala_df            
            lala_df=plot_df
            
            
            for j in range(np.shape(plot_df)[1]):
                             
 
                ax=None
                
                if viv_seq==True:   
                    try:
                        ax=plot_df.iloc[:,j].plot(style=style_list[i][j],color=color_list[i][j],fontsize=12,ax=ax)
                       
                    except IndexError:
                        ax=plot_df.iloc[:,j].plot(style=None,color=None,fontsize=12,ax=ax)
                else :
                    ax=plot_df.iloc[:,j].plot(style=None,color=None,fontsize=12,ax=ax)
                     
            i+=1
              
        #plt.rcParams.update({ytick.minor.size: 15})
        #plt.rcParams['font.size']=25
        

       
        
        #plt.rcParams.update({'font.size': 22})
        plt.title(sec.attrs['TITLE'],fontsize=25)
        plt.xlabel(sec.attrs['XLABEL'],fontsize=20)
        plt.ylabel(sec.attrs['YLABEL'],fontsize=20)
        #plt.xticks(fonsize=20)
        #plt.yticks(fonsize=20)
        plt.legend(bbox_to_anchor=(1,1.01), loc='upper left', ncol=1,fontsize=15)
        
        
        if viv_seq==True:
            savetitle='Figure'+str(plot_num)    
            plot_num+=1
        else:
            #Delete all non decinumeric characters and replace them with blankspace
            savetitle=re.sub('[^0-9a-zA-Z]+', '_',sec.attrs['TITLE'])
            
        
        savetitle=output_path+savetitle
        plt.savefig(savetitle+'.png',bbox_inches='tight',dpi=400)
        plt.draw()        
        #plt.show(block=True)
        plt.close()
      
      
   
    '''
    -----------------------------
    --------MAIN PROGRAM---------
    -----------------------------
    '''
    
    #path='C:\\Users\\janp\\Documents\\datasets\\exp_viv_nan\\plot\\'
    
    #exp_matr= mp.MpfFile(path+'omae2010_10xx_res_experiment_v2.mpf')
    #viv_matr=mp.MpfFile(path+'omae2010_10xx_res_vivana_v2.mpf')
    #exp_matr=mp.MpfFile('riflex_nc.mpf')
    if mpf_list==None:
        mat_names=filenames(path,'.mpf')
    else:
        mat_names=mpf_list       
        
    
    mat_list=[mp.MpfFile(path+name) for name in mat_names]

        
    #Create a list that includes all "Matrix" names of all mpf files
    all_keys_list=[list(mat_list[i].keys()) for i in range(len(mat_list))]
    all_keys=pd.DataFrame(all_keys_list, index=mat_names)
    
    #Create  a dataframe, describing which matrix titles are included in the mpf files.
    check=check_keys(all_keys)
    
    print('Overview of all matrices in the imported ".mpf" files:\n',check)

    #each step of this loop creates one plot
    global plot_num
    plot_num=1
    for key in check.index.tolist():
            
        boo_list=check.loc[key]
        
        plot_list_mat=[]
        for i in range (len(boo_list)):
            if boo_list[i]:
                plot_list_mat.append(mat_list[i])
        
        plot_list_sec=[plot_list_mat[j][key] for j in range(len(plot_list_mat)) ]        
        
        mat_plt_list(plot_list_sec)
        
        #---------Scatterplots--------------
        
        #error=test_for_scatter(plot_list_sec)
        if (True in check.loc[key].values) and scatter:
            for i in range (len( plot_list_sec[0].yvalues)): #Skips between IL and CF
                exp_y_vals,viv_y_new, exp_y_label , viv_y_label = scat.extract_and_interpolate(plot_list_sec,i)
                scat.scatter_plot(output_path,plot_list_sec ,exp_y_vals,viv_y_new, exp_y_label , viv_y_label)
    
   
        
    
    #np.squeeze(list(dict(hj).values()))[1]