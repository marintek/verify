# -*- coding: utf-8 -*-
"""
Created on Fri May 20 13:53:21 2016

@author: janp
"""

def renderer(tex_path,tex_name,work_path,out_path,client=None):
    import string
    import os
    from shutil import copyfile
    
    #Change working directory
    os.chdir(work_path)    
    
    copylist=['myabbrvnat.bst','Vivana_bib.bib']
    for name in copylist:
        copyfile(tex_path+'\\'+name,work_path+'\\'+name)
        

    
    tex_name=tex_name.replace('.tex','')
    
    class LaTeXTemplate(string.Template):
        delimiter="???"
    
    #rend_file=open (work_path+'\\'+tex_name+'_rendered.tex','w')
    
    with open (tex_path+'\\'+tex_name+'.tex','r') as template:
        data=template.read()
        
        with open (work_path+'\\'+tex_name+'_rendered.tex','w') as rendered:
            text=LaTeXTemplate(data)
            rendered.write(text.substitute(client=client))
            rendered.close()
            
    template.close()
    
    
    for i in range(3):  #normal range 3!!
        if i==1:
            os.system('bibtex '+tex_name+'_rendered')
        os.system('xelatex -interaction=nonstopmode '+tex_name+'_rendered')
        os.system('%& -output-directory='+out_path)
        
    
    copyfile(work_path+'\\'+tex_name+'_rendered.pdf', out_path+'\\'+tex_name+'_rendered.pdf')
    
    os.startfile(out_path+'\\'+tex_name+'_rendered.pdf')