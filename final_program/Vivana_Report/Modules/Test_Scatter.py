# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 14:08:54 2016

@author: janp
"""

'''
This program creates an mpf- file , with which the scatter plot function
of "command_plot.py" can be tested.
Use the file "Vivana.mpf", which was created by this program and create a scatter
plot with "command_plot.py"
The plotted data should be 100% correlated.
'''


import numpy as np
import miopyc as mp
import os
import sys

wdir=os.getcwd()
sys.path.insert(0, 'C:\\Users\\janp\\Documents\\Python Scripts\\mpf generator')

import Create_Mpf_Class as scat

def rand_autocorr(leng):
    
    out=[]
    
    for i in range(leng):
        m=True
        while m:
            r=np.random.uniform(low=0, high=2, size=1)

            if r>0:
                m=False
                
        out.append(np.float(r))
        
    return np.array(out)

def fun(x):
    y=[]
    for val in x:
        
        res=5.0*val+9.0
        y.append(res)
        
    return y
    

exp_x=rand_autocorr(8)
exp_x= np.sort(exp_x)
viv_x=rand_autocorr(29)
viv_x=np.sort(viv_x)


exp_y=fun(exp_x)
viv_y=fun(viv_x)


exp_sec=scat.section( 'Series 5 Test','Series 5 Test','Experiment curr','Experiment y',{'Experiment IL':exp_x},{'Experiment IL':exp_y})

viv_sec=scat.section( 'Series 5 Test','Series 5 Test','Vivana curr','Vivana y',{'CF':viv_x},{'CF':viv_y} )

exp_mpf=scat.Mpf('Test Exp')

exp_mpf.append(exp_sec)
exp_mpf.print_it('Experiment','Head')



viv_mpf=scat.Mpf('Test Viv')

viv_mpf.append(viv_sec)
viv_mpf.print_it('Vivana','Head')
#viv_sec=scat.section()









