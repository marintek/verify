% Poster class for SINTEF with LaTeX.
%
% by Federico Zenith, federico.zenith@sintef.no.

\ProvidesClass{sintefposter}[2011/09/23 Poster class for SINTEF]

% Language options
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{sciposter}}
\ProcessOptions\relax

\LoadClass[36pt,plainsections]{sciposter}

% Load SINTEF-specific graphic elements
\RequirePackage{sintefcolor}

% Set font to Calibri, or Times clone for serif
\RequirePackage{fontspec}
\defaultfontfeatures{Ligatures=TeX}
\setmainfont{TeX Gyre Termes}
\setsansfont{Calibri}

\definecolor{mainCol}{named}{white}
\definecolor{BoxCol}{named}{sinteflightgray}
\definecolor{TextCol}{named}{sintefblue}
\definecolor{SectionCol}{named}{sintefblue}

% Sets color as well as size
\renewcommand{\titlesize}{\huge\color{sintefblue}}

\leftlogo{Sintef_logo_blue}
