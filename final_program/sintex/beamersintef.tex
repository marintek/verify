\documentclass{beamer}
\usepackage{amsfonts,amsmath,oldgerm,xltxtra}

\usetheme{sintef}
\usefonttheme[onlymath]{serif}

\newcommand{\hrefcol}[2]{\textcolor{cyan}{\href{#1}{#2}}}

\title{An Introduction to the SINTEF Beamer Style}
\subtitle{Using \LaTeX\ to prepare slides}
\author{\href{mailto:federico.zenith@sintef.no}{Federico Zenith}}
\institute{Department of Proper Typesetting}
\date{Written on December 16, 2010}
\titlegraphic{
  \huge
  \includegraphics[width=0.5\columnwidth]{Sintef_logo_blue}\\
  +\\{\rm\LaTeX}\\= sant
}

\setcornertext{\LaTeX\ for better presentations}

\begin{document}

\maketitle

\begin{frame}
\frametitle{Beamer for SINTEF slides}
\begin{itemize}
\item We assume you can use \LaTeX; if you cannot,
\hrefcol{http://en.wikibooks.org/wiki/LaTeX/}{you can learn it here}
\item Beamer is one of the most popular and powerful document
classes for presentations in \LaTeX
\item Beamer has also a detailed
\hrefcol{http://www.ctan.org/tex-archive/macros/latex/contrib/beamer/doc/beameruserguide.pdf}{user manual}
\item Here we will present only the most basic features to get you up to speed
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Beamer vs. PowerPoint}
Compared to PowerPoint, using \LaTeX\ is better because:
\begin{itemize}
\item It is not What-You-See-Is-What-You-Get, but
What-You-\emph{Mean}-Is-What-You-Get:\\
you write the content, the computer does the typesetting
\item Produces a \texttt{pdf}: no problems with fonts, formulas,
      program versions
\item Easier to keep consistent style, fonts, highlighting, etc.
\item Math typesetting in \TeX\ is the best:
\begin{equation*}
\mathrm{i}\,\hslash\frac{\partial}{\partial t} \Psi(\mathbf{r},t) =
-\frac{\hslash^2}{2\,m}\nabla^2\Psi(\mathbf{r},t)
+ V(\mathbf{r})\Psi(\mathbf{r},t)
\end{equation*}

\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Compilation}
Files must be compiled with \XeLaTeX, which produces \texttt{pdf} files
directly.

\begin{block}{Compiling with \XeLaTeX}
\verb|xelatex my_presentation.tex|
\end{block}

If you use some sort of GUI for \LaTeX\ (e.g. Kile on Linux,
\TeX{}maker on Windows) you may have to define an appropriate
compilation action.
\end{frame}

\begin{frame}[fragile]
\frametitle{Selecting the Theme}
To start working with Beamer, start a \LaTeX\ document with the
preamble:
\begin{block}{Minimum SINTEF Beamer Preamble}
\begin{verbatim}
\documentclass{beamer}
\usetheme{sintef}\end{verbatim}
\end{block}
You may want to set your institute's acronym or the SINTEF motto
in the lower right corner; the default is the English motto.
\begin{block}{Setting the lower-right corner text}
\verb|\setcornertext{Technology for a better society}|
\end{block}
\end{frame}

\begin{frame}[fragile]
\frametitle{Setting a title page}
To set a typical title page, you call some commands in the preamble:
\begin{block}{The Commands for the Title Page}
\begin{verbatim}
\title{Sample Title}
\subtitle{Sample subtitle}
\author{First Author \and Second Author}
\institute{The name of your institute or group}
\date{Defaults to today's}
\titlegraphic{A second column on the right}
\end{verbatim}
\end{block}
You can then write out the title page with \verb|\maketitle|.
\end{frame}

\begin{frame}[fragile]
\frametitle{Writing a Simple Slide}
\framesubtitle{It's really easy!}
\begin{itemize}[<+->]
\item A typical slide has bulleted lists
\item These can be uncovered in sequence
\end{itemize}
\begin{block}{Code for a Page with an Itemised List}<+->
\begin{verbatim}
\begin{frame}
  \frametitle{Writing a Simple Slide}
  \framesubtitle{It's really easy!}
  \begin{itemize}[<+->]
    \item A typical slide has bulleted lists
    \item These can be uncovered in sequence
  \end{itemize}
\end{frame}\end{verbatim}
\end{block}
\end{frame}

\begin{frame}[fragile]
\frametitle{Using Colours}
\begin{itemize}[<alert@2>]
  \item You can use colours with the
        \verb|\textcolor{<color name>}{text}| command
  \item The colours are defined in the \texttt{sintefcolor} package:
  \begin{itemize}
    \item Primary palette: \textcolor{sintefblue}{sintefblue},
    \textcolor{cyan}{cyan}, \textcolor{black}{black},
    \textcolor{sintefdarkgrey}{sintefdarkgrey},
    \textcolor{sinteflightgrey}{sinteflightgrey}
    \item Secondary palette: \textcolor{sinteflightblue}{sinteflightblue},
    \textcolor{sintefdarkgreen}{sintefdarkgreen},
    \textcolor{sinteflightgreen}{sinteflightgreen},
    \textcolor{sintefyellow}{sintefyellow}, \textcolor{sintefred}{sintefred},
    \textcolor{sintefbrown}{sintefbrown}, \textcolor{magenta}{magenta}
  \end{itemize}
  \item Do \emph{not} abuse colours: \verb|\emph{}| is usually enough
  \item Use \verb|\alert{}| to bring the \alert<2->{focus} somewhere
  \item<2- | alert@2> If you highlight too much, you don't highlight at all!
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Adding images}
\begin{columns}
\begin{column}{0.7\textwidth}
Adding images works like in normal \LaTeX:
\begin{block}{Code for Adding Images}
\begin{verbatim}
\usepackage{graphicx}
% ...
\includegraphics
[width=\textwidth]{Sintef_logo_blue}
\end{verbatim}
\end{block}
\end{column}
\begin{column}{0.3\textwidth}
\includegraphics
[width=\textwidth]{Sintef_logo_blue}\\
\end{column}
\end{columns}
\end{frame}

\begin{frame}[fragile]
\frametitle{Splitting in Columns}
Splitting the page is as easy as it is commonplace;
typically, one side has a picture and the other text:
\begin{columns}
\begin{column}{0.6\textwidth}
This is the first column
\end{column}
\begin{column}{0.3\textwidth}
And this the second
\end{column}
\end{columns}
\begin{block}{Column Code}
\begin{verbatim}
\begin{columns}
    \begin{column}{0.6\textwidth}
        This is the first column
    \end{column}
    \begin{column}{0.3\textwidth}
        And this the second
    \end{column}
    % There could be more!
\end{columns}
\end{verbatim}
\end{block}
\end{frame}

\begin{frame}[fragile]
\frametitle{Fonts}
\begin{itemize}
\item The paramount task of fonts is being readable
\item There are good ones...
  \begin{itemize}
  \item {\textrm{Use serif fonts only with high-definition projectors}}
  \item {\textsf{Use sans-serif fonts otherwise (or if you simply prefer them)}}
  \end{itemize}
\item ... and not so good ones:
  \begin{itemize}
  \item {\texttt{Never use monospace for normal text}}
  \item {\frakfamily Gothic, calligraphic or weird fonts: should always: be
  avoided}
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Good Luck!}
\begin{itemize}
\item Enough for an introduction! You should know enough by now
\item If you have corrections or suggestions,
\hrefcol{mailto:federico.zenith@sintef.no}{send them to me!}
\end{itemize}
\end{frame}

\end{document}
