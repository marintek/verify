% Generic class for SINTEF documents with LaTeX.
%
% by Federico Zenith, federico.zenith@sintef.no.

\ProvidesClass{sintefdoc}[2014/07/08 A generic class for SINTEF documents]

% Correct a number of bugs in LaTeX2e
\RequirePackage{fixltx2e}
\RequirePackage{etoolbox,ltxcmds}
% Command to provide alternative translations in English and Norwegian
\newcommand{\EnglishNorsk}[2]{\ifdefstring{\languagename}{norsk}{#2}{#1}}

% We are not using AMS packages here, but they must be loaded before polyglossia
% calls Xunicode and causes conflicts. Furthermore, unicode-math should
% also be loaded "after any other maths- or font-related package".
% Amsmath and amssymb are very important packages for scientific usage,
% so we are willing to include them always.
\RequirePackage{amsmath,amssymb}
\RequirePackage{unicode-math}
\unimathsetup{math-style=ISO,bold-style=ISO, % ISO standards for italic & bold
              nabla=upright, partial=upright, % Operators should be upright
}

% Make Polyglossia shut up when selecting norwegian versions
\RequirePackage{silence}
\WarningFilter{polyglossia}{gloss-norsk.ldf is already loaded!}

\RequirePackage{polyglossia}
% Default language is (British) English
\setmainlanguage[variant=british]{english}
\setotherlanguage{norsk}

\providebool{useISBN}

% Classification options, internal is default as per SINTEF policy
\newcommand*{\@classification}{\EnglishNorsk{Internal}{Intern}}
\DeclareOption{unrestricted} {%
  \renewcommand*{\@classification}{\EnglishNorsk{Unrestricted}{Åpen}}%
  \booltrue{useISBN}% In case of public reports
}
\DeclareOption{internal}{%
  \renewcommand*{\@classification}{\EnglishNorsk{Internal}{Intern}}%
  \boolfalse{useISBN}% No need for ISBN in internal documents
}
\DeclareOption{restricted}{%
  \renewcommand*{\@classification}{\EnglishNorsk{Restricted}{Fortrolig}}%
  \boolfalse{useISBN}% No point for ISBN in restricted documents
}
\DeclareOption{confidential}{%
  \renewcommand*{\@classification}{\EnglishNorsk{Confidential}{Strengt fortrolig}}%
  \boolfalse{useISBN}% No point for ISBN in confidential documents
}

% \and function for multiple authors
\renewcommand{\and}{\\*}

% SINTEF's division. Default is SINTEF foundation.
\newcommand{\@division}{SINTEF}

% Whether to change the front page layout for digital signatures
\providebool{digitalsignature}
% Default: stay on the manual system
\boolfalse{digitalsignature}

% Language options
\DeclareOption{english}  {\setmainlanguage[variant=british]{english}}
\DeclareOption{engelsk}  {\setmainlanguage[variant=british]{english}}
\DeclareOption{norsk}    {\setmainlanguage                 {norsk}}
\DeclareOption{norwegian}{\setmainlanguage                 {norsk}}
\DeclareOption{marintek} {\renewcommand{\@division}{MARINTEK}}
\DeclareOption{digital}  {\booltrue{digitalsignature}}
\DeclareOption{manual}   {\boolfalse{digitalsignature}}
\DeclareOption{twocolumn}{\OptionNotUsed}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

\LoadClass[a4paper,11pt]{article}
% Sectsty is used in many child classes, though not sintefdoc.
% We require it here anyway because it should come before
% \setmathfont, that redefines \underbar and makes sectsty output
% irritating warnings.
\RequirePackage{sectsty}

% Define the main logo (MARINTEK or SINTEF)
\ifdefstring{\@division}{MARINTEK}{%
  \newcommand{\@mainlogo}{Marintek_logo_blue}
  \newcommand{\@website}{www.marintek.sintef.no}
}{
  \newcommand{\@mainlogo}{Sintef_logo_blue}
  \newcommand{\@website}{www.sintef.no}
}

% Use sans font by default
\RequirePackage{sintefcolor}
\RequirePackage{fontspec}
\defaultfontfeatures{Ligatures=TeX}
% If we use font names, XeLaTeX might not find the font, esp. in Linux,
% see http://tex.stackexchange.com/questions/132906/.
\setmainfont[
	Extension = .otf,
	UprightFont = *-regular,
	BoldFont = *-bold,
	ItalicFont = *-italic,
	BoldItalicFont = *-bolditalic
]{texgyretermes}
% math font using unicode-math
\setmathfont{texgyretermes-math.otf}

% WORKAROUND For some reason, \setminus is not present in TeX Gyre Termes.
%            Therefore we import it from Asana, until it will be implemented
%            in TGTermes too.
%            These lines may then be removed.
\setmathfont[range=\setminus]{Asana-Math.otf}

% The default sans-serif font is Calibri, but it is not available on Linux.
% As an alternative, we use Asap that is quite similar (less so in italics)
% and freely available from http://www.fontsquirrel.com/fonts/asap.
% The font test below is from http://tex.stackexchange.com/questions/53443/
\suppressfontnotfounderror=1
\def\testFontName{"Calibri"}
\font\testFont=\testFontName\space at 10pt
\ifx\testFont\nullfont% Calibri not found - test Asap
	\def\testFontName{"Asap"}
	\font\testFont=\testFontName\space at 10pt
	\ifx\testFont\nullfont% Asap not found - stop
		\PackageError{sintefdoc}{SINTeX needs Calibri or Asap font}{See sintefdoc.pdf}
	\else% Asap found - use it, but issue an worning
		\PackageWarning{sintefdox}{Using Asap font instead of Calibri}
		\setsansfont[Scale=0.915]{Asap}
	\fi
\else % Calibri found - use it
	\setsansfont{Calibri}
\fi
\suppressfontnotfounderror=0
%
\renewcommand{\familydefault}{\sfdefault}
\sffamilylatin % Polyglossia is in the bad habit of messing with font families

% command to print one line of the header list, if the argument is non-empty
% [#1] = separator between text and argument (default = ": ")
% {#2} = the argument to check
% {#3} = the text printed if the argument is non-empty
\newcommand{\headListItem}[3][: ]{\ifdefempty{#2}{}{#3#1#2}}
% command to end a line in a list if there are some more non-empty items
% this is needed to avoid empty lines in tabular lists (below)
% (ifdefempty works only for single commands!)
\newcommand{\lineBreakIf}[1]{\ifdefstring{#1}{}{}{\\}}
% command to print one line of the tabular list, if the argument is non-empty
% [#1] = arguments of the following lines (so we can check if we need '\\')
% {#2} = the argument
% {#3} = the text preceding the argument
\newcommand{\headTabItem}[3][]{\ifdefempty{#2}{}{#3: & #2\lineBreakIf{#1}}}

\RequirePackage{sinteflabels}
\InputIfFileExists{sintefconfig}{}{%
  \ClassError{sintefdoc}{%
  The sintefconfig.tex file is missing.\MessageBreak
  You can generate it by double-clicking sintefconfig.js.
  }{%
  The sintefconfig.tex file contains various information\MessageBreak
  about you and your department, including phone numbers,\MessageBreak
  addresses and the like.\MessageBreak
  \space Double-clicking sintefconfig.js in a SINTEF Windows PC\MessageBreak
  generates sintefconfig.tex just as it should be for you.
  }%
}

% The information box, longer with location (physical address)
\newcommand{\@longinfo}[1][sintefblue]{%
  \noindent%
  \footnotesize%
  \raggedright%
  \setlength{\parskip}{1ex}%
  \color{#1}%
  \ifdefstring{\languagename}{norsk}{%
    \textbf{\@institute}
  }{%
    \textbf{\textnorsk{\@institute}}\\
    \@institute
  }

  \headListItem[:\\]{\@address}{\@addresslabel}

  \headListItem[:\\]{\@location}{\@locationlabel}

  \ifdefstring{\@phone\@direct\@fax}{}{}{% only if some item is non-empty
    \begin{tabular}{@{}lr}
      \headTabItem[\@direct\@fax]{\@phone}{\@phonelabel}
      \headTabItem[\@fax]{\@direct}{\@directlabel}
      \headTabItem[]{\@fax}{\@faxlabel}
    \end{tabular}
  }

  \@email\lineBreakIf{\@email}
  \@website\lineBreakIf{\@website}
  \headListItem{\@VAT}{\@VATlabel}

  \ifdefstring{\@division}{MARINTEK}{%
    \vspace{1ex}
    \includegraphics[width=0.8\textwidth]{Sintef_logo_blue}
  }{}
}

% The information box, shorter.
\newcommand{\@shortinfo}[1][sintefblue]{%
  \noindent%
  \footnotesize%
  \raggedright%
  \setlength{\parskip}{1ex}%
  \color{#1}%
  \ifdefstring{\languagename}{norsk}{%
    \textbf{\@institute}
  }{%
    \textbf{\textnorsk{\@institute}}\\
    \@institute
  }

  \headListItem[:\\]{\@address}{\@addresslabel}

  \ifdefstring{\@phone\@fax}{}{}{% only if some item is non-empty
  \begin{tabular}{@{}lr}
    \headTabItem[\@fax]{\@phone}{\@phonelabel}
    \headTabItem[]{\@fax}{\@faxlabel}
  \end{tabular}
  }

  \@email\lineBreakIf{\@email}
  \@website\lineBreakIf{\@website}
  \headListItem{\@VAT}{\@VATlabel}

  \ifdefstring{\@division}{MARINTEK}{%
    \vspace{1ex}
    \includegraphics[width=0.8\textwidth]{Sintef_logo_blue}
  }{}
}

% This separating line is used across several documents,
% for example in footers and title pages.
\RequirePackage{sintefcolor}
\newcommand{\@separator}{%
  % To make sure we have spacing on both sides, make an invisible rule, 2X tall
  \rule{0ex}{2ex}%
   % Place the dashed rule 1X high
  \textcolor{sintefdarkgray}{\rule[1ex]{\textwidth}{0.25pt}}%
}
