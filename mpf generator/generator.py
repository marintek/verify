# -*- coding: utf-8 -*-
"""
Created on Wed Jun  1 11:54:56 2016

@author: janp
"""

import numpy as np
import pandas as pd
import pydoc
import miopyc as mp
from os import listdir
from os.path import isfile, join
import os
import sys

wdir=os.getcwd()
sys.path.insert(0, wdir)

import Create_Mpf_Class as cmc

def generate_mpf(in_path,out_path=None):
    
    #out_path='C:\\Users\\janp\\Documents\\test_python\\test_files\\'
    #in_path='C:\\Users\\janp\\Documents\\datasets\\raw vivana\\use'

    
    wdir=os.getcwd()
    
    if out_path==None:
        out_path=wdir
    in_path+='\\'
    out_path+='\\'
   
    
    def filenames(path,file_name_cont):
        if path==None:
            onlyfiles = [f for f in listdir(path) if isfile(f)]
        else:   
            onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
            
        chosen_files=[]
        
        for name in onlyfiles:
            append=True
            for cont in file_name_cont:
                if not cont in name:
                    append=False
        
            if append:
                chosen_files.append(name)
            
        return chosen_files

    '''
    arg_find_closest(lis,val)
    finds closest value in lis to val and returns its position in lis
    '''
    def arg_find_closest(lis,val):
        diff=[]
        for i in range(len(lis)):
            diff.append(np.abs(lis[i]-val))
        pos=np.argmin(diff)
        return pos
    '''
    number_crossing(arr) 
    counts how many times there is a change of the algebraic sign in the array
    '''    
    def number_crossing(arr):
        count=0
        positive=[]
        for i in range(len(arr)):
            if arr[i]>=0:
                positive.append(True)
            else:
                positive.append(False)
        
        for i in range(len(positive)-1):
            if not positive[i]==positive[i+1]:
                count+=1
        return count
    
    '''
    calc_mode(sec_list, freq, flow)
    calculates the mode 
    '''
    def calc_mode(sec_list, freq, flow):
        if flow.lower()=='il':
            harm=2
        elif flow.lower()=='cf':
            harm=1
            
        keys=list(sec_list['1D modes, harm. '+str(harm)].yvalues.keys())
        keys_flt=[key.replace('Hz','') for key in keys ]
        keys_flt=[key.replace(' ','') for key in keys_flt ]
        keys_flt=[np.float(key) for key in keys_flt ]
        
        
        pos=arg_find_closest(keys_flt,freq)
        key=keys[pos]
        d_mode=sec_list['1D modes, harm. '+str(harm)].yvalues[key]
        
        '''The number of crossings has to be added with one in order to get the mode'''
        mode=number_crossing(d_mode)+1 
        
        return mode
        
        
        
    def import_values(name,path):
        sec_list=mp.MpfFile(path+name)
        freq_vals=sec_list['Max response amplitudes'].xvalues
        amp_vals=sec_list['Max response amplitudes'].yvalues
        amp_vals=np.squeeze(list(dict(amp_vals).values()))
        if not len(freq_vals)==2:
            print('not exactly 2 response frequencies')
        '''
        #Assumption, that cf_frequ is always smaller than il_freq... ASSUMPTIONS 
        MAY CAUSE WRONG DATA WHEN TESTING VIVANA DATA
        '''
        cf_pos=np.argmin(freq_vals)  
        il_pos=np.argmax(freq_vals)
        
        cf_freq=freq_vals[cf_pos]
        il_freq=freq_vals[il_pos]
        
        cf_amp=amp_vals[cf_pos]
        il_amp=amp_vals[il_pos]
        
        diam=sec_list['Shedding frequency, diameter, current velocity and Strouhal number'].yvalues['Diam.'][0]
        curr_vels=sec_list['Current velocity'].yvalues['Cur.vel.']
        curr_vel=np.max(curr_vels)
        
        try:
            cf_mode=calc_mode(sec_list, cf_freq, 'cf')
            il_mode=calc_mode(sec_list, il_freq, 'il')
        
        except:
            print('There occured an error when calculating the mode in file '+name
            +' \nthe mode is set to "nan"')
            cf_mode=np.nan
            il_mode=np.nan
            
            
       
        
        return curr_vel,diam,cf_freq,il_freq,cf_amp,il_amp,cf_mode,il_mode


    
    def calc_amp(amp,diam):
        ad=amp/diam
        return ad
        

            
    
    '''
    ---------MAIN PROGRAM---------
    '''
    
    
    
        
    files=filenames(in_path,['R3','vivana','.mpf'])
      
    series_num=''
    
    if 'S' in files[0]:
        series_num=files[0][1:5].replace('X','')
    
    out_name='Series'+series_num
    out_file_name=out_path+out_name
    
    
    
    out_xval={"'curr_vel'":[]}
    out_yval={"'cf_freq'":[],"'il_freq'":[],"'cf_AD'":[],"'il_AD'":[],"'cf_mode'":[],"'il_mode'":[]}
    
    '''
    The key names in out_xval and out_yval are a relict from an older version of this program. In fact 
    the mpf module is looking for the terms 'il' or 'cf' in the key name in order to name the legend either
    'Vivana cf' or 'Vivana il'
    '''
    
    
    for name in files:
        try:
            
            curr_vel,diam,cf_freq,il_freq,cf_amp,il_amp,cf_mode,il_mode=import_values(name,in_path)
            
            cf_AD=calc_amp(cf_amp,diam)
            il_AD=calc_amp(il_amp,diam)
        
            out_xval["'curr_vel'"].append(curr_vel)
            out_yval["'cf_freq'"].append(cf_freq)
            out_yval["'il_freq'"].append(il_freq)
            out_yval["'cf_AD'"].append(cf_AD)
            out_yval["'il_AD'"].append(il_AD)
            out_yval["'cf_mode'"].append(cf_mode)
            out_yval["'il_mode'"].append(il_mode)
            
        except:
            print('MatrixPlotfile '+name+' was skipped due to an error')
    
    mpf_file=cmc.Mpf('Test')
    
    
    amp_diam_dict={ key: out_yval[key] for key in ["'cf_AD'","'il_AD'"] }
    amp_diam_sec=cmc.section('Series '+series_num+': A/D','Series '+series_num+': A/D','Current [m/s]','A/D [-]',out_xval,amp_diam_dict)
    mpf_file.append(amp_diam_sec)
    
    
    freq_dict={ key: out_yval[key] for key in ["'cf_freq'","'il_freq'"] }
    freq_sec=cmc.section('Series '+series_num+': Freq','Series '+series_num+': Freq','Current [m/s]','Freq. [Hz]',out_xval,freq_dict) 
    mpf_file.append(freq_sec)
    
    mode_dict={ key: out_yval[key] for key in ["'cf_mode'","'il_mode'"] }
    mode_sec=cmc.section('Series '+series_num+': Mode','Series '+series_num+': Mode','Current [m/s]','Mode',out_xval,mode_dict) 
    mpf_file.append(mode_sec)
    
    
    mpf_file.print_it(out_file_name,'Head')
    

                
        