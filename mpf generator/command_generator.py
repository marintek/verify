# -*- coding: utf-8 -*-
"""
Created on Fri Jun 10 18:59:20 2016

@author: janp
"""

import sys
import argparse
import os

wdir=os.getcwd()
sys.path.insert(0, wdir)

import generator as gen

parser=argparse.ArgumentParser(description=
'''
This program takes in a series of ".mpf" files created by Vivana.
It then automatically creates an ".mpf" file containing all information
needed to create the "Vivana Report" which is comparing the Vivana results
to tests
'''
)
parser.add_argument('-i',"--in_path",help=
'''
Type in the path, that contains the Vivana Series. The Program will pick out
the files, that it needs automatically
'''
)
parser.add_argument('-o',"--out_path",help=
'''
Type in the path, where the newly generated file should be saved.
If this command is not used the file will be saved in the working directory
'''
)
args=parser.parse_args()


gen.generate_mpf(args.in_path, args.out_path)