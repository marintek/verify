--------------
##############
README
##############
--------------
The executable file is called "command_generator.py" and has to be executed in the command line. 
When using the command "-h" an overview about all commands is provided.

The command -i has to be used in order to define the input path.


This program takes in a series of ".mpf" files created by Vivana.
It then automatically creates an ".mpf" file containing information
needed to create the "Vivana Report" which is comparing the Vivana results
to tests. 

The filename is expected to be in the following format:
"S10XX_0030_R3_vivana.mpf"
An set of raw Vivana output files can be found in the subfolder "raw vivana"

Those files in the input folder are selected, which contain all of the following terms:
"R3", "vivana", ".mpf"

The output file consits of two matrices. The first matrix is showing the amplitude devided by the diameter
with increasing current velocity while the second is showing the frequency with increasing current velocity. 

The output file is named after the number of the series.



Please test the program results before using them! Especially the mode calculation was not tested yet!