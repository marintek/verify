# -*- coding: utf-8 -*-
"""
Created on Fri Jun 10 19:26:31 2016

@author: janp
"""
import numpy as np
import pandas as pd

'''
This class is used to write Mpf files
First of all you have to create section objects.
Then create an Mpf object. Use its "append" command to
append all sections to your mpf object.
Use the "print_it" command in order to create the new
mpf file.
''' 

class Mpf(object):
    def __init__(self,heading) :
        sections=[]
        self.heading=heading
        self.sections=sections
    def append(self,sec):
        self.sections.append(sec)    
    def print_it(self,filename,heading):
        def spaces(s,total):
            spaces=total-len(s)
            ret=s+(spaces*' ')
            return ret  
            
        file=open(filename+'.mpf','w')
        head_line=spaces('HEADING',10)+heading+'\n'
        file.write(head_line)
        for sec in self.sections:
            for line in sec.print_form():
                file.write(line+'\n')
        file.close()

class section(object):
    def __init__(self, matrix,title,xlabel,ylabel,xvalues,yvalues):
        self.matrix=matrix
        self.title=title
        self.xlabel=xlabel
        self.ylabel=ylabel
        self.xvalues=xvalues
        self.yvalues=yvalues 
        
    def print_form(self):
        def spaces(s,total):
            spaces=total-len(s)
            ret=s+(spaces*' ')
            
            return ret  
            
        print_total=[]
        df1=pd.DataFrame.from_dict(self.xvalues)
        df2=pd.DataFrame.from_dict(self.yvalues)
        df=pd.concat([df1, df2], axis=1, join='inner')
        
        print_total.append(spaces('MATRIX',10)+self.matrix)
        print_total.append(spaces('TITLE',10)+self.title)
        print_total.append(spaces('XLABEL',10)+self.xlabel)
        print_total.append(spaces('YLABEL',10)+self.ylabel)
        
        legends_str=''
        for name in df.keys():
            
            if ('il' in name) or ('Il' in name) or ('IL' in name):
                l_name="'VIVANA IL'"
            elif ('cf' in name) or ('Cf' in name) or ('CF' in name):
                l_name="'VIVANA CF'"
            else:
                l_name=name
            
            legends_str+=l_name+' '
        
        print_total.append(spaces('LEGENDS',10)+legends_str)
        
            
        col_ind=0
        for col in range(np.shape(df)[0]):
        
            
            print_row=''
            row_ind=0
            for row in range(np.shape(df)[1]):
                val_to_app=str(df.iloc[col,row])
                if row_ind==0:
                    if col_ind==0:
                        print_row+=(spaces('VALUES',10)+val_to_app+'  ')
                    else:
                        print_row+=(spaces('',10)+val_to_app+'  ')
                else:
                    print_row+=(val_to_app)+'  '
                    
                    
                
                row_ind+=1
            
            #print_row+='\n'
            print_total.append(print_row)
            col_ind+=1

        return print_total